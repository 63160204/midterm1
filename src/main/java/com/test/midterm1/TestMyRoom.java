/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.midterm1;

/**
 *
 * @author patthamawan
 */
public class TestMyRoom {

    public static void main(String[] args) {

        Doll1 doll1 = new Doll1("Name is Bobo", "Color is Gray", "-> Dog");
        System.out.println(doll1.name + "|" + doll1.color + " " + doll1.type);

        Doll2 doll2 = new Doll2("Name is Mumu", "Color is White", "-> Troll");
        System.out.println(doll2.name + "|" + doll2.color + " " + doll2.type);

        Doll3 doll3 = new Doll3("Name is Hoybrab", "Color is White,pink,green", "-> Shellfish");
        System.out.println(doll3.name + "|" + doll3.color + " " + doll3.type);

        Doll4 doll4 = new Doll4("Name is Tahwan", "Color is purple", "-> Octopus");
        System.out.println(doll4.name + "|" + doll4.color + " " + doll4.type);
        
        Doll5 doll5 = new Doll5("Name is BanyBear", "Color is white", "-> Bear");
        System.out.println(doll5.name + "|" + doll5.color + " " + doll5.type);
    }
}
